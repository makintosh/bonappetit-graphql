const CLIENT_ENDPOINTS = {
    GETALL: '/getAll',
    SAVE: '/save',
    GET_BY_ID: '/getById',
};

const HTTP_STATUS = {
    SUCCESS: 200,
    BAD_REQUEST: 400,
    INTERNAL_ERROR: 500
};

module.exports = { CLIENT_ENDPOINTS, HTTP_STATUS };
const { DB } = require("../../configuration/database");
const { sql } = require("slonik");
const { Client } = require("./client.model.js");
const {
  castToClassArr,
  castObjectToClass,
} = require("../../util/main.util.js");

/**
 * @author atello-barcia
 */
exports.clientController = {
  /**
   * Get all clients
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  getAll() {
    return new Promise(
      (resolve, reject) => {
        setTimeout(async () => {
          try {
            console.log("inside timeout");
            const data = (
              await DB.pool.query(sql`
                          select 
                              * from cliente`)
            ).rows;
            const casted = castToClassArr(data, CONSTANT.fields, Client);
            resolve(casted);
          } catch (err) {
            console.log(err);
            reject(err);
          }
        }, 8000);
      },
      (err) => console.log(err)
    );
  },

  /**
   *
   */
  getById: ({ ci }) => {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        const data = await DB.pool.one(sql`
                  select 
                  * from cliente where cli_ced = ${ci}`);
        resolve(castObjectToClass(data, CONSTANT.fields, Client));
      }, 5000);
    });
  },

  /**
   *
   * @param {*} param0
   */
  create({ client }) {
    try {
      console.log(client);
    } catch (err) {
      console.log(err);
    }
  },
};

const CONSTANT = {
  fields: [
    "cli_ced__ci",
    "cli_nom__name",
    "cli_ape__lastName",
    "cli_dir__address",
    "cli_tlf__mobilePhone",
    "cli_ema__email",
  ],
};

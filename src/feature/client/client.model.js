class Client {
    mobilePhone;
    name;
    constructor(address, ci, email, lastName) {
        this.address = address;
        this.ci = ci;
        this.email = email;
        this.lastName = lastName;
    }
}

module.exports = { Client };

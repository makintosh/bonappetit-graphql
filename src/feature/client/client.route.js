const express = require("express");
const { clientController } = require("./client.controller");
const { CLIENT_ENDPOINTS, HTTP_STATUS } = require("./client.constant");

const clientRouter = express.Router();

clientRouter.route(CLIENT_ENDPOINTS.GETALL).get((req, res, next) => {
  clientController.getAll()
    .then((response) => res.status(200).send(response))
    .catch((err) => next(err));
});

clientRouter.route(CLIENT_ENDPOINTS.GET_BY_ID).get((req, res, next) => {
    clientController.getById(req.query)
      .then((response) => res.status(200).send(response))
      .catch((err) => next(err));
  });

module.exports = clientRouter;

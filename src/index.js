const express = require('express');
const port = process.env.PORT || 4000;
const graphqlHttp = require('express-graphql');
const { buildSchema } = require('graphql');
const { clientController } = require('./feature/client/client.controller.js');

const app = express();

const clientRouter = require('./feature/client/client.route');

app.use(express.json({ limit: '100mb' }));
app.use((req, res, next) => {
    console.log('Tiemout middleware')
    res.setTimeout(7000, () => {
        console.log('timeout completed');
        res.send(408);
    });
    next();
});

const clientSchema = buildSchema(`
    type Client {
        ci: String!
        name: String
        lastName: String
        address: String
        mobilePhone: String
        email: String
    }

    input ClientInput {
        ci: String!
        name: String
        lastName: String
        address: String
        mobilePhone: String
    }
    
    type Query {
        getAll: [Client!]!
        getById(ci: String!): Client!
    }

    type Mutation {
        create(client: ClientInput): Client
    }

`);

app.use('/graphql', graphqlHttp({
    schema: clientSchema,
    rootValue: clientController,
    graphiql: true,
    
}));

app.use('/client', clientRouter);

app.use((err, req, res, next) => {
    console.log('Error middleware');
    res.status(500).send('Internal server error');
});

app.listen(port, 'localhost', () => {
    console.log('Server running at :', port);
});

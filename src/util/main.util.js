/**
 * @author atello-barcia, jcedeno
 * Cast to model and returns
 * @param response any[]
 * @param dbColumnsNameToModelNames string[]
 * @param model className
 */
exports.castToClassArr = (response, dbColumnsNameToModelNames, model) => {
    const data = [];
    if (response.length) {
        response.forEach(row => {
            const p = new model();
            for (let name of dbColumnsNameToModelNames) {
                const x = name.split('__');
                p[x[1]] = row[x[0]];
            }
            data.push(p);
        });
    }
    return data;
}

exports.castObjectToClass = (response, dbColumnsNameToModelNames, model) => {
    let p = {};
    if (response) {
        p = new model();
        for (let name of dbColumnsNameToModelNames) {
            const x = name.split('__');
            p[x[1]] = response[x[0]];
        }
    }
    return p;
}

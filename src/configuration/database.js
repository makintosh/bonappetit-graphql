const { createPool } = require('slonik');

const POSTGRES = {
    URI: 'postgres://eideas:eideas@localhost:5433/bonappetit',
};

class DB {
    static pool = createPool(POSTGRES.URI);
}

module.exports = { DB };
